<?php
    require_once('animal.php');
    require_once('ape.php');
    require_once('frog.php');

    $sheep = new Animal("Shaun The Sheep");
    echo "Name : " . $sheep->name."<br>";
    echo "Legs : " . $sheep->legs."<br>";
    echo "Cold_blooded : " . $sheep->cold_blooded."<br>";
    echo "Rumpun : " . $sheep->jenis."<br>";
    echo "Jenis kelamin : " . $sheep->jk."<br><br>";

    $sungokong = new Ape("Kera Sakti Mandaraguna");
    echo "Name : " . $sungokong->name."<br>";
    echo "Legs : " . $sungokong->legs."<br>";
    echo "Cold_blooded : " . $sungokong->cold_blooded."<br>";
    echo "Rumpun : " . $sungokong->jenis."<br>";
    echo "Jenis kelamin : " . $sungokong->jk."<br>";
    echo $sungokong->yell("Auooo");
    echo "<br><br>";

    $kodok = new Frog("Kodok Budug");
    echo "Name : " . $kodok->name."<br>";
    echo "Legs : " . $kodok->legs."<br>";
    echo "Cold_blooded : " . $kodok->cold_blooded."<br>";
    echo "Rumpun : " . $kodok->jenis."<br>";
    echo "Jenis kelamin : " . $kodok->jk."<br>";
    echo $kodok->jump("Hop-hop");
    
?>