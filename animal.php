<?php
    class Animal{
        public $name;
        public $legs = 2;
        public $cold_blooded = "no";
        public $jenis = "Herbivora";
        public $jk = "Jantan";

        public function __construct($string){
            $this->name = $string;
        }
    }

?>