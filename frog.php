<?php
require_once("animal.php");

class Frog extends Animal{
    public $legs = 4;
    public $jenis = "Karnivora";
    public $jk = "Betina";
    
    public function jump($string){
        echo "Jump : " . $string;
    }
}

?>